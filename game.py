from random import randint
from tkinter.messagebox import YES

name = input("Hi, what is your name?")

for guess_number in range(1,6):
    month_guess = randint(1,12)
    year_guess = randint(1924,2004)

    print("Guess", guess_number, ": ", name, "were you born in", month_guess, "/", year_guess, "?")

    answer = input("yes or no?")

    if answer == "yes":
        print("I knew it!")
        exit()

    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
        exit()
    else:
        print("Drat! Lemme try again!") 


